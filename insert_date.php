<!DOCTYPE html>
<?php
session_start();
if (!isset($_SESSION['admin'])) {
	header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
	exit();
}

include("include/connexion.php");
require("class/Year.php");

?>
<html lang="fr">
<head>
	<title>Insérer une date associé à un règne</title>
	<?php
	include("include/header.php");// Notre page admin de connexion
	?>
</head>
<body>
  	<div class="container enleveMarge">
		<div class="row">
			<br>
		</div>
		<div class="row">
			<div class="col-4 text-left">
				<a href="addElement.php"><button class="btn btn-primary">Revenir à la page des options</button></a>
			</div>
			<div class="col-4 text-center">
				<h5> Ajout de date historique dans la base de donnée </h5>
			</div>
			<div class="col-4"></div>
		</div>
		<div class="row">
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
		<div class="row">
			<div class="col-4 text-left">

				<?php
					$string = "";
					if (!empty($_POST['year']) && !empty($_POST['reign'])) {
						// Vérification si cette année est déjà présente dans la BDD 

						$dejaPresent = false;
                        $sql = "SELECT `year` FROM `year`";
                        $result = $dbh->query($sql)->fetchAll();
                        for ($i = 0;$i < count($result);$i++){
                            if ($result[$i]['year'] == $_POST['year']){
                                $dejaPresent = true;
                            }
						}

						if ($dejaPresent){
							$string = "\nCette année est déjà présente dans la BDD, veuillez en choisir une autre"; 
						} else {
							$year = new Year($_POST['year'], $_POST['reign']);
							$year->insert();
							$string = "insertion réussie";
						}

					} else if (!empty($_POST['year'])){
						$string = "\nVeuillez remplir le reigne correspondant à l'année que vous voulez rentrer";
					} else if (!empty($_POST['year'])){
						$string = "\nVeuillez remplir l'année que vous voulez ajouter";
					}
				
					// Gestion de la suppression
					if (!empty($_POST['date']) && !empty($_POST['supprimer'])){

						$date = $_POST['date'];
						$del = $dbh->prepare("DELETE FROM `year` WHERE `year` = '$date'");
						try{
							if($del->execute()){

							}
						}
						catch(Exception $e){
							echo "<p>" . $e->getMessage() . "</p>";
						}
					}

					// Modification 
					if (!empty($_POST['validerModif'])) {
						$request = "UPDATE `year` SET `year` = '$_POST[newDate]',reign = '$_POST[newReign]' WHERE `year` = '$_POST[oldDate]'";
						$dbh->exec($request);
					}
				?>
			
				<!-- Ici afficher les dates déjà présente dans la BDD -->
				Liste des années dans la BDD <br>
				<form method="POST"> 
					<?php

						$requete = "SELECT `year`,`reign` FROM `year`";
						$result = $dbh->query($requete)->fetchAll();
						for ($i = 0; $i < count($result); $i++){
							echo $result[$i]['year']. ", reigne de ";
							echo $result[$i]['reign']. "     ";
							$date = $result[$i]['year'];
							echo "<input type='radio' name='date' value='$date'><br>";
						}
					?>
					<br>
					<input type='submit' value='Modifier' name ="modifier">
					<input type='submit' value='Supprimer' name="supprimer">
				</form>
				<br>
				<?php
					if (!empty($_POST['date']) && !empty($_POST['supprimer'])){
						echo "Suppression réussie";
					}
				?>

			</div>
			<div class="col-4">
				<form action="insert_date.php" method="post">
					<p>
						Insérer une année dans la base de données <br />
						<input type="number" name="year" max="2020"/> Année <br />
						<input type="text" name="reign"/> Reigne <br />
				
					</p>
					<p>
						<input type="reset" name="reset" value="Effacez" />
						<input type="submit" name="submit" value="Validez" />
					</p>
				</form>
				<?php
					echo $string;
				?>
			</div>
			<div class="col-4">
				<?php 
					if (!empty($_POST['date']) && !empty($_POST['modifier'])){
						// Gérer la modification
				?>
						<form method="post">
							<p>
							Modifier un objet historique dans la base de donnée <br>
							<?php 
								$sql = "SELECT reign FROM `year` WHERE `year` = $_POST[date]";
								$result = $dbh->query($sql)->fetch();
							?>
							<input type="hidden" name="oldDate" value="<?php echo $_POST['date'] ?>">
							<input type="text" name="newDate" value="<?php echo $_POST['date'] ?>"/> Nom de l'objet <br>
							<input type="text" name="newReign" value="<?php echo $result['reign'] ?>"> Reigne <br>
							</p>
							<p>
								<input type="submit" name="validerModif" value="Validez les modifications" />
							</p>
						</form>
				<?php
					}
				?>
			</div>
		</div>
    </div>
</body>
<?php
	include("include/footer.php");
?>
</html>

