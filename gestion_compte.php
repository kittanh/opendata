<!DOCTYPE html>
<?php
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
    exit();
}
require_once("include/connexion.php");

?>
<html>
<head>
    <title></title>
    <?php
    include("include/header.php");// Notre page admin de connexion
    ?>
</head>
<body>

    <div class="row">
        <br>
    </div>
    <div class="row">
        <div class="col-4">
            <p>Séléctionnez l'utilisateur</p>
        </div>
        <div class="col-4">

        </div>
        <div class="col-4 text-center">
            <a href="admin.php"><button class="btn btn-primary">Retour</button></a>
        </div>
    </div>
    <form action="gestion_compte.php" method="post">

        <?php
        $requete = "SELECT idU, login, name, surname, email, type FROM user";
        $resultat = $dbh->query($requete);
        $resultat->setFetchMode(PDO::FETCH_ASSOC);
        ?>

        <div class="row">
            <div class="col-1 normalGras">
                Identifiant
            </div>
            <div class="col-1 normalGras">
                Login
            </div>
            <div class="col-1 normalGras">
                Prénom
            </div>
            <div class="col-1 normalGras">
                Nom
            </div>
            <div class="col-2 normalGras">
                Email
            </div>
            <div class="col-1 normalGras">
                Type
            </div>
        </div>

        <?php 
            while($ligne = $resultat->fetch()){
                $string = '<div class ="row">
                    <div class="col-1">
                        '.$ligne['idU'].'
                    </div>
                    <div class="col-1">
                        '.$ligne['login'].'
                    </div>
                    <div class="col-1">
                        '.$ligne['name'].'
                    </div>
                    <div class="col-1">
                        '.$ligne['surname'].'
                    </div>
                    <div class="col-2">
                        '.$ligne['email'].'
                    </div>
                    <div class="col-2">';

                if($ligne['type'] == 1) $string .= 'admin';
                else $string .= 'contributeur';
                $string .= '</div>';
                $string .= '<input type="radio" name="idU" value="'.$ligne['idU'].'"/></div>';
                echo $string;
            }
            $resultat->closeCursor();
        ?>
        <br>
        <p>
            <input type="submit" name="action" value="Modifier" />
            <input type="submit" name="action" value="Supprimer" />
        </p>

    </form>

</body>
<?php
include("include/footer.php");
?>
</html>

<?php
if(isset($_POST['idU'])){
    //modifier
    if ($_POST['action'] == 'Modifier') {
        $request = "SELECT idU, login, name, surname, email, type FROM user where idU = $_POST[idU]";
        $result = $dbh->query($request);
        $user = $result->fetch();

        $idU = $_POST['idU'];
        if ($user['type'] == 1) {
            $option1 = "admin";
            $option2 = "contributeur";
            $value2 = 0;
        } else {
            $option2 = "admin";
            $option1 = "contributeur";
            $value2 = 1;
        }
        echo "<h3>Détails de l'utilisateur $user[login]</h3>";
        echo "
                <form action='update_user.php' method='POST'>
                <table>
                    <tr>
                        <th>Identifiant</th>
                        <td>$user[idU]<input type='hidden' name='idU' value='$user[idU]'</td>
                    </tr>
                    <tr>
                        <th>Login</th>
                        <td><input type='text' name='login' required value='$user[login]'></td>
                    </tr>
                    <tr>
                        <th>Prénom</th>
                        <td><input type='text' name='name' maxlength='50' required value='$user[name]'></td>
                    </tr>
                    <tr>
                        <th>Nom de famille</th>
                        <td><input type='text' name='surname' maxlength='50' required value='$user[surname]'></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><input type='text' name='email' maxlength='50' required value='$user[email]'></td>
                    </tr>
                    <tr>
                        <th>Type</th>
                        <td>
                            <select name='type'> 
                                <option value='$user[type]'>$option1</option>
                                <option value='$value2'>$option2</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><input type='reset' value='Annuler les modifications'></td>
                        <td><input type='submit' value='Mettre à jour les données'></td>
                    </tr>
                    
            
                    
                </table>
                </form>
                ";
    } else if ($_POST['action'] == 'Supprimer') {
        $idU = $_POST['idU'];
        $del = $dbh->prepare("DELETE FROM `user` WHERE idU LIKE (:num)");
        try{
            if($del->execute(array(':num' => $idU))){
                echo "DELETE OK";
            }
        }
        catch(Exception $e){
            echo "<p>" . $e->getMessage() . "</p>";
        }
    }
}
?>