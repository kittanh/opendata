<!DOCTYPE html>
<?php

session_start();
if (!isset($_SESSION['admin'])) {
	header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
	exit();
}


include("include/connexion.php");
require("class/Mapp.php");

?>

<html>
<head>
	<title></title>
	<?php
		include("include/header.php");
	?>
</head>
<body>
	<?php 
		
		$scandir = scandir("admin/upload");
		/*
		if (count($scandir) == 2) {
			echo "Vous n'avez aucune carte à upload, veuillez revenir ici quand vous en aurez une";
			header('Refresh:1; url=addElement.php');
			exit;
		}
		*/
	?>
  	<div class="container enleveMarge">
		<div class="row">
			<br>
		</div>
		<div class="row">
			<div class="col-4 text-left">
				<a href="addElement.php"><button class="btn btn-primary">Revenir à la page des options</button></a>
			</div>
			<div class="col-4 text-center">
				<h5> Upload de carte </h5>
			</div>
			<div class="col-4"></div>
		</div>
		<div class="row">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
		<div class="row">
			<div class="col-4">
				<?php
					$string = "";
					if (!empty($_POST['level']) && !empty($_POST['date']) && !empty($_POST['map']) && isset($_POST['source'])){

						// Recherche si cette carte est déjà présente dans la BDD ou bien si l'année et l'étage ne le sont pas

						$dejaPresent = false;
						$sql = "SELECT `level`,`year`,`name` FROM map NATURAL JOIN `year`";
						$result = $dbh->query($sql)->fetchAll();
						for ($i = 0;$i < count($result);$i++){
							if ($result[$i]['level'] == $_POST['level'] && $result[$i]['year'] == $_POST['date']){
								$dejaPresent = true;
								$string = "\nIl y à déjà une carte pour cette année et cette étage là";
							} else if ($result[$i]['name'] == $_POST['map']){
								$dejaPresent = true;
								$string = "\nIl y à déjà une carte avec ce nom là";
							}
						}
						if (!$dejaPresent) {

							$file = $_POST['map'];
							$dirDestination = 'image/plan/';
							$dirSource = 'admin/upload/';

							if (rename($dirSource.$file, $dirDestination.$file)){

								$string = "Votre plan a bien été transféré";
								header("Refresh:2");
							} else {
								$string = "Erreur de déplacement de ".$dirSource.$file."vers ".$dirDestination.$file;
							}
							
							$map = new Mapp($_POST['level'], $_POST['date'],$file,$_POST['source']);
							$map->insert();
						}
					}

					// Gestion de la suppression
					if (!empty($_POST['name']) && !empty($_POST['supprimer'])){

						$date = $_POST['name'];
						$del = $dbh->prepare("DELETE FROM `map` WHERE `name` = '$name'");
						try{
							if($del->execute()){

							}
						}
						catch(Exception $e){
							echo "<p>" . $e->getMessage() . "</p>";
						}
					}

					// Modification 
					if (!empty($_POST['validerModif'])) {

						$dejaPresent = false;
						$sql = "SELECT `level`,`year`,`name` FROM map NATURAL JOIN `year`";
						$result = $dbh->query($sql)->fetchAll();
						for ($i = 0;$i < count($result);$i++){
							if ($result[$i]['level'] == $_POST['newLevel'] && $result[$i]['year'] == $_POST['newYear']){
								$dejaPresent = true;
								$string = "\nIl y à déjà une carte pour cette année et cette étage là, vous ne pouvez pas faire cette modification";
							}
						}

						if (!$dejaPresent){
							$sql = "SELECT idY FROM `year` WHERE `year` = $_POST[newYear]";
							$newIdYear = $dbh->query($sql)->fetch();
							$request = "UPDATE `map` SET `level` = '$_POST[newLevel]',`idY` = $newIdYear[idY],source = '$_POST[newSource]' WHERE `name` = '$_POST[name]'";
							$dbh->exec($request);
						}
					}
					
				?>

				Liste des map dans la BDD <br>
				<form method="POST"> 
					
					<?php

						$requete = "SELECT `name`,`level`,`year` FROM map NATURAL JOIN `year`";
						$result = $dbh->query($requete)->fetchAll();
						for ($i = 0; $i < count($result); $i++){
							echo $result[$i]['name'];
							echo ", étage " . $result[$i]['level'];
							echo ", année " . $result[$i]['year'] . "    ";
							$name = $result[$i]['name'];
							echo "<input type='radio' name='name' value='$name'><br>";
						}
					?>
					<br>
					<input type='submit' value='Modifier' name ="modifier">
					<input type='submit' value='Supprimer' name="supprimer">
				</form>
				<br>
				<?php
					if (!empty($_POST['name']) && !empty($_POST['supprimer'])){
						echo "Suppression réussie";
					}
				?>
			</div>
			<div class="col-4">
				<form action="insert_map.php" method="POST">
				<p>
					Insérer une map dans la base de données <br />
					<select name ="level">
						<option value="RDC">RDC</option>
						<option value="Etage1">Etage 1</option>
						<option value="Etage2">Etage 2</option>
					</select>
					Etage
					</br>
					<select name="date"> 
					<?php
						$sql = "SELECT `year` FROM `year`";
						$sth = $dbh->query($sql); 
						//$row = $sth->fetch_assoc();
						$result = $sth->fetchAll();
								for($i = 0;$i<count($result);$i++){
					?>
						<option value="<?php echo $result[$i][0]; ?>"><?php echo $result[$i][0]; ?></option>
					<?php 
						}
					?>
					</select>
					Date
									</br>
					<?php
						//$scandir = scandir("admin/upload");
						//print_r($scandir);
					?>
					<select name="map">
						<?php 
							foreach($scandir as $fichier){
								if ($fichier != ".." && $fichier != "."){
								?>
									<option value="<?php echo $fichier ?>"><?php echo $fichier ?></option>
							<?php
								}
							}
						?>
					</select>
					Nom du fichier carte
					</br>
					<input type="text" name="source"> Source de la carte
				</p>
				<p>
					<input type="reset" name="reset" value="Effacez" />
					<input type="submit" name="submit" value="Validez" />
				</p>
				</form>
				<br>
				<?php
					echo $string;
				?>
			</div>
			<div class="col-4">
				<?php 
					if (!empty($_POST['name']) && !empty($_POST['modifier'])){
						// Gérer la modification
				?>
						<form method="post">
							<p>
							Modifier les dépendances de la carte dans la base de donnée <br>
							<?php 
								$sql = "SELECT `year`,`level`,source FROM `map` NATURAL JOIN `year` WHERE `name` = '$_POST[name]'";
								$result = $dbh->query($sql)->fetch();
							?>
							<input type="text" name="name" readonly value="<?php echo $_POST['name'] ?>"> Nom du fichier de la carte
							<br>
							<select name="newYear">
								<?php 
									$sql = "SELECT `year` FROM `year`";
									$sth = $dbh->query($sql); 
									$years = $sth->fetchAll();

									for ($i = 0; $i < count($years);$i++){

										if ($result['year'] == $years[$i][0]){
								?>
											<option selected value="<?php echo $years[$i][0]; ?>"><?php echo $years[$i][0]; ?></option>
								<?php
										}
										else {
								?>
											<option value="<?php echo $years[$i][0]; ?>"><?php echo $years[$i][0]; ?></option>
								<?php
										}
									}
								?>
							</select> Date
							<br>
							<select name="newLevel">
								<?php 
									if($result['level'] == "RDC"){
								?>
										<option selected value="RDC">RDC</option>
								<?php 
									} else {
								?>
										<option value="RDC">RDC</option>
								<?php
									}
									
									if($result['level'] == "Etage1"){
								?>
										<option selected value="Etage1">Etage 1</option>
								<?php 
									} else {
								?>
										<option value="Etage1">Etage 1</option>
								<?php
									}
									if($result['level'] == "Etage2"){
								?>
										<option selected value="Etage2">Etage 2</option>
								<?php
									} else {
								?>
										<option value="Etage2">Etage 2</option>
								<?php
									}
								?>
							</select> Etage
							<br>
							<input type="text" name="newSource" value="<?php echo $result['source'] ?>"> Source de la carte
							</p>
							<p>
								<input type="submit" name="validerModif" value="Validez les modifications" />
							</p>
						</form>
				<?php
					}
				?>
			</div>
		</div>
	</div>
	<?php



	?>
</body>
<?php
	include("include/footer.php");
?>
</html>

