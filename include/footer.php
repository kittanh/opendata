    <!-- Footer -->
<footer class="page-footer fixed-bottom font-small black">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">
      
      <div class="col-md-1">
        
      </div>
      <!-- Grid column -->
      <div class="col-md-2 mx-auto">

        <!-- Links -->
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4 dore">Crédits</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="white">Very long link 1</a>
          </li>
          <li>
            <a href="#!" class="white">Very long link 2</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <div class="col-md-2 mx-auto">

        <!-- Links -->
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4 dore">Mentions légales</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="white">Link 1</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <!-- Grid column -->
      <div class="col-md-2 mx-auto">

        <!-- Links -->
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4 dore">En savoir plus</h5>

        <ul class="list-unstyled">
          <li>
            <a href="https://bitbucket.org/kittanh/opendata/src/master/" class="white">En savoir plus</a>
          </li>
          <li>
            <a href="#!" class="white">Link 2</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <!-- Grid column -->
      <div class="col-md-2 mx-auto">

        <!-- Links -->
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4 dore">Links</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="white">Link 1</a>
          </li>
          <li>
            <a href="#!" class="white">Link 2</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="color:white">Copyright © 2020 x Inc. Tous droits réservés
  </div>
  <!-- Copyright -->

</footer>