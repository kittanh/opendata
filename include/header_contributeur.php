<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Immersailles</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </head>
  <body>

<header>
      <!-- Image and text -->
    
     <nav class="navbar navbar-dark black">
          
        <ul class="nav">
          <li>
            <a class="navbar-brand" href="#">
              <img src="image/logo_mini.png" width="50" height="50" class="d-inline-block align-middle" alt="" loading="lazy"> 
              <i> IMMERSAILLES </i>
            </a>
          </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;" >
                    Menu
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="insert_history_object.php">Insertion d'objet historique</a>
                    <a class="dropdown-item" href="insert_open_data.php">Insertion de lien open data</a>
                    <a class="dropdown-item" href="insert_marker.php">Insertion de marker sur une carte</a>
                </div>
            </li>
        </ul>
        


          <!-- <span class="navbar-text white">
            Immersailles
          </span>
      -->
          
            <!-- <div class="navbar-text"> -->
              <!-- Changez les images pour mettre des ronds blancs pour symboliser la connexion -->



      <div>

        <a href="admin_connexion.php"><span class="dot d-inline-block align-middle"></span></a>
  
  
        <span class="dot d-inline-block align-middle"></span>
      </div>   
        
               
            
            <!-- </div> -->
         
    </nav>
  
</header>
