<?php
class Open_data {
    private $id;
    private $url;
    private $name_object;
    
    public function __construct($url, $name_object) {
        $this->id = explode("/", $url)[4];
        $this->url = $url;
        $this->name_object = $name_object;
    }
    
    // Ne pas oublier de limiter à 10 liens maximun un objet historique
    public function insert() {
        require_once("connexion.php");
        $name_object = $this->name_object;
        $select = $dbh->query("SELECT idO FROM `objet` WHERE name = '$name_object'");
        $result = $select->fetch();
        
        $insert = $dbh->prepare("INSERT INTO `opendata` (`idOP`, `url`, `idO`) VALUES (:uidOP, :uurl, :uidO);");
        
        try{
            if($insert->execute(array(':uidOP' => $this->id, ':uurl' => $this->url, ':uidO'=> $result[0]))){
                echo "Insertion réussie\n<br>";
            }
        }
        catch(Exception $e){
            echo "<p>" . $e->getMessage() . "</p>";
        }
        
    }

    public function getObjet() {

        $json = file_get_contents("https://www.wikidata.org/wiki/Special:EntityData/" . $this->id . ".json");
        $objet = json_decode($json);
        return $objet;
    }

    public function getImage($objet) {
        $id = $this->id;
        $img = $objet->entities->$id->claims->P18[0]->mainsnak->datavalue->value;
        //cast le type, mettre (string)
        //var_dump($img);
        $img = str_replace(" ", "_", $img);
        $md5 = md5($img);
        $link = "https://upload.wikimedia.org/wikipedia/commons/thumb/" . $md5[0] . "/"
            . substr($md5, 0, 2) . "/" . $img . "/700px-" . $img;
        return $link;
    }

    public function getName($objet) {
        $id = $this->id;
        return $objet->entities->$id->labels->fr->value;
    }

    public function getGender($objet) {
        $id = $this->id;
        $idS = $objet->entities->$id->claims->P21[0]->mainsnak->datavalue->value->id;
        $json = file_get_contents("https://www.wikidata.org/wiki/Special:EntityData/" . $idS . ".json");
        $objS = json_decode($json);

        return $objS->entities->$idS->labels->fr->value;
    }

    public function getDateBirth($objet) {
        setlocale(LC_TIME, "fr_FR");
        $id = $this->id;
        $date = $objet->entities->$id->claims->P569[0]->mainsnak->datavalue->value->time;
        return strftime("%d %B %G", strtotime($date));
    }

    public function getDateDeath($objet) {
        setlocale(LC_TIME, "fr_FR");
        $id = $this->id;
        $date = $objet->entities->$id->claims->P570[0]->mainsnak->datavalue->value->time;
        return strftime("%d %B %G", strtotime($date));
    }

    public function getDescription($objet) {
        $id = $this->id;
        return $objet->entities->$id->descriptions->fr->value;
    }

    public function getLink($objet) {
        $id = $this->id;
        $link = $objet->entities->$id->sitelinks->frwiki->url;
        return "<a href = \"" . $link . "\"> Wikipedia </a>";
    }

    public function __toString() {
        return "url = " . $this->url . " <br/> Nom de l'objet = " . $this->name_object;

    }
}

?>
