<?php
class Year {
  private $year;
  private $reign;

  public function __construct($year, $reign) {
    $this->year = $year;
    $this->reign = $reign;
  }

  public function insert() {
      require_once("connexion.php");

      $insert = $dbh->prepare("INSERT INTO `year` (`year`, `reign`) VALUES (:uyear, :ureign);");
      
      try{
        if($insert->execute(array( ':uyear' => $this->year, ':ureign'=> $this->reign))){

        }
      }
      catch(Exception $e){
        echo "<p>" . $e->getMessage() . "</p>";
      }
      
  }

  public function __toString() {
    return $this->year + " " + $this->reign;
  }

  public function getYear() {
    return $this->year;
  }
}

?>
