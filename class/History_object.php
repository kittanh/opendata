<?php
class History_object {
    private $name;
    private $type;
    
    public function __construct($name, $type) {
        $this->name = $name;
        $this->type = $type;
    }
    
    public function insert() {
        require_once("connexion.php");
        
        $insert = $dbh->prepare("INSERT INTO `objet` (`name`, `type`) VALUES (:uname, :utype);");
        
        try{
            if($insert->execute(array( ':uname' => $this->name, ':utype'=> $this->type))){
                return "Insertion réussie <br>";
            }
        }
        catch(Exception $e){
            return "<p>" . $e->getMessage() . "</p>";
        }
    
    }
    
    public function __toString() {
        return "Nom de l'objet = " . $this->name . " <br/> Type de l'objet = " . $this->type;
    }
}

?>
