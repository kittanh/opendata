<?php
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
    exit();
}
include("include/header.php");
?>

    <body>
        <div class="container enleveMarge">
            <div class="row">

                <!-- Affichage de outils et interactifs -->
                <div class="col-1">
                    <div class="row">
                        <br>
                    </div>
                    <a href="admin.php"><button class="btn btn-primary">Retour</button></a>
                </div>
                <div class ="col-2 text-right">
                    <div class="row">
                        <br>
                    </div>
                    
                    <div class="row outils">
                        <div class="col-12 text-right">
                            O
                        </div>
                    </div>
                    <div class="row outils">
                        <div class="col-12 text-right">
                            U
                        </div>
                    </div>
                    <div class="row outils">
                        <div class="col-12 text-right">
                            T
                        </div>
                    </div>
                    <div class="row outils">
                        <div class="col-11 text-right">
                            I
                        </div>
                    </div>
                    <div class="row outils">
                        <div class="col-12 text-right">
                            L
                        </div>
                    </div>
                    <div class="row outils">
                        <div class="col-12 text-right">
                            S
                        </div>
                    </div>
                </div>

                <div class="col-2 margeNegatif">
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <br>
                        <br class="demiBR">
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            I
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            N
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            T
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            E
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            R
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            A
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            C
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            T
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            I
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            F
                        </div>
                    </div>
                    <div class="row interactif">
                        <div class="col-3 text-center">
                            S
                        </div>
                    </div>
                </div>
                <!-- Fin affichage outils -->


                <!-- Les bouttons -->
                <div class="col-5 text-center">
                    <div class="row">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>

                    </div>
                    <div class="row">
                        <div class="col-lg text-left">
                            <a href="insert_history_object.php"><button class="btn btn-primary">Insertion d'objet historique</button></a>
                        </div>
                        <div class="col-lg text-right">
                            <a href="insert_open_data.php"><button class="btn btn-primary">Insertion de lien open data</button></a>
                        </div>
                    </div>
                    <div class="row">

                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-lg text-center">
                            <a href="insert_map.php"><button class="btn btn-primary">Insertion de carte</button></a>
                        </div>
                    </div>
                    <div class="row">

                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-lg text-left">
                            <a href="insert_date.php"><button class="btn btn-primary">Insertion de date historique</button></a>
                        </div>
                        <div class="col-lg text-right margeBoutonBasDroite">
                            <a href="insert_marker.php"><button class="btn btn-primary">Insertion de marqueur sur une carte</button></a>
                        </div>
                    </div>
                </div>
                <!-- Fin bouttons -->
            </div>
        </div>
        
    </body>
    <br>
    <br>
    <br>
    <?php
   		include("include/footer_index.php");
   	?>
</html>