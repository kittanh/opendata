<!DOCTYPE html>
<?php
    session_start();
    if (!isset($_SESSION['admin']) && !isset($_SESSION['contri'])) {
		header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
		exit();
	}
    include("include/connexion.php");
    include("class/Open_data.php");

    if (isset($_SESSION['admin'])) {
        $page = "addElement.php";
    } else {
        $page = "contributeur.php";
    }

?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Leaflet map</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>

  </head>
  <body>

<?php include('include/header_contributeur.php') ?>
<body>
    <div class="row">
        <br>
    </div>
    <div class="row decalageDroite">
        <div class="col-3">
            Sur quelle carte voulez vous mettre vos markers ? <br>
            <form method="POST">
                Carte 
                <select name="map">
                    <option value="">---Carte---</option>
                    <?php
                        $sql = "SELECT `name` FROM `map`";
                        $sth = $dbh->query($sql); 
                        $result = $sth->fetchAll();
                        for($i = 0;$i<count($result);$i++){
                    ?>        
                            <option value="<?php echo $result[$i][0]; ?>"><?php echo $result[$i][0]; ?></option>
                    <?php } ?>
                </select> <br>
                <input type="submit" value="Valider" id="mapName">
            </form>
        </div>
        <div class="col-7"></div>
        <div class="col-2">
            <a href="<?php echo $page ?>"><button class="btn btn-primary">Revenir à la page des options</button></a>
        </div>
    </div>
    
    <?php 
        if (!empty($_POST['X']) && !empty($_POST['Y']) && !empty($_POST['object']) && isset($_POST['ajouter'])){
            $x = $_POST['X'];
            $y = $_POST['Y'];
            $map = $_POST['map'];
            $object = $_POST['object'];
            //$creatorName = $_POST['creator'];
            //$creationDate = $_POST['creationDate'];

            $insert = $dbh->prepare("INSERT INTO `marker` (`x`, `y`,`idO`,`idMap`) VALUES (:ux, :uy,:uidO,:umap);");

            $select = $dbh->query("SELECT idO FROM `objet` WHERE `name` = '$object'");
            $idO = $select->fetch();

            $select = $dbh->query("SELECT idMap FROM `map` WHERE `name` = '$map'");
            $idMap = $select->fetch();

            try{
                if($insert->execute(array( ':ux' => $x, ':uy'=> $y, ':uidO' => $idO['idO'],':umap' => $idMap['idMap']))){
                    echo "Insertion réussie";
                }
            }
            catch(Exception $e){
                echo "<p>" . $e->getMessage() . "</p>";
            }
        ?>
    
        <?php
        } else if (isset($_POST['supprimer'])){
                        
            // Bien supprimer de la bdd mais à mettre tout en haut pour éviter le chargement de chiasse
            // Peut poser problème si la personne décide de changer des infos puis de supprimer, normalement personne ne fait ça, mais penser à le gérer

            $x = $_POST['X'];
            $y = $_POST['Y'];
            $map = $_POST['map'];

            $select = $dbh->query("SELECT idMap FROM `map` WHERE `name` = '$map'");
            $idMap = $select->fetch();
            $id = $idMap['idMap'];

            $delete = $dbh->prepare("DELETE FROM `marker` WHERE x = $x AND y = $y AND idMap = $id");

            try{
                if($delete->execute()){
                    echo "Suppression réussie";
                }
            }
            catch(Exception $e){
                echo "<p>" . $e->getMessage() . "</p>";
            }

        } else if (isset($_POST['modifier'])){
            
            // Mettre ici les actions de la modifications d'un marker donc pour l'instant juste avec son objet historique
            $x = $_POST['X'];
            $y = $_POST['Y'];
            $map = $_POST['map'];
            $object = $_POST['object'];

            $select = $dbh->query("SELECT idMap FROM `map` WHERE `name` = '$map'");
            $idMap = $select->fetch();
            $id = $idMap['idMap'];

            $select = $dbh->query("SELECT idO FROM `objet` WHERE `name` = '$object'");
            $result = $select->fetch();
            $idO = $result['idO'];

            // Trouver un moyen de trouver la bonne ligne à modifier
            $select = $dbh->query("SELECT idM FROM marker WHERE x=$x AND y=$y AND idMap=$id");
            $result = $select->fetch();
            $idMarker = $result['idM'];

            $update = $dbh->prepare("UPDATE `marker` SET `x`=$x,`y`=$y,`idO`=$idO WHERE idM = $idMarker");
            
            try{
                if($update->execute()){
                    echo "Suppression réussie";
                }
            }
            catch(Exception $e){
                echo "<p>" . $e->getMessage() . "</p>";
            }


            

        } else if (isset($_POST['object'])){
        // L'affichage d'un message d'erreur en cas de mauvaise utilisation du client pose soucis garder ça pour plus tard
            ?>
                <div class="alert alert-warning" role="alert">
                <?php //echo "Vous devez renseignez un objet historique pour un marker donné"; // on affiche le msg  ?>
            </div>
        <?php
        }
        if (isset($_POST['map'])){

            $fileMap = $_POST['map'];
            $sql = "SELECT `x`,`y`,idO FROM `marker`,map WHERE marker.idMap = map.idMap && map.name = '$fileMap'";
            $sth = $dbh->query($sql); 
            $result = $sth->fetchAll();
    ?>
            <div id="map"></div>
            <script type="text/javascript">

                var bounds = [[0,0], [400,1000]];
                var map = L.map('map',{
                    crs: L.CRS.Simple,
                    minZoom:1,
			        maxBounds:bounds
                });

                <?php 
                echo "var image = L.imageOverlay('image/plan/$fileMap', bounds).addTo(map);";
                ?>

                
                var coolIcon = L.icon({
                    iconUrl: 'image/icon.png'
                });
                

                    var coord = [];
                    map.setView([200,500],0);

                <?php
                    
                    for($i = 0;$i<count($result);$i++){
                        $x = $result[$i]['x'];
                        $y = $result[$i]['y'];
                        $idO = $result[$i]['idO'];

                        $sql = "SELECT `name` FROM objet WHERE idO = $idO";
                        $info = $dbh->query($sql)->fetch();
                        $nameNonFormel = $info['name'];
                    
                ?>

                        coord = [<?php echo $y ?>,<?php echo $x ?>];
                        
                <?php
                        $sql = "SELECT `url` FROM opendata WHERE idO = $idO";
                        $res = $dbh->query($sql)->fetch();
                        $url = $res['url'];

                        $od = new Open_data("$url", "$nameNonFormel");
                        $objet = $od->getObjet();
                        $img = $od->getImage($objet);
                        $name = $od->getName($objet);
                        $sexe = $od->getGender($objet);
                        $dateNaissance = $od->getDateBirth($objet);
                        $dateMort = $od->getDateDeath($objet);
                        $description = $od->getDescription($objet);
                        $link = $od->getLink($objet);

                        
                ?>      

                        var mapElem = document.getElementById("map");
                        L.marker(coord,{icon:coolIcon}).addTo(map).on("click",function(e){
                            console.log("<?php echo $description ?>");
                            if (tab.length == 1) {
                                var oldMarker = tab[0];
                                map.removeLayer(oldMarker);
                            }
                            
                            var banderol = document.getElementById('banderol');

                            if (banderol == null) mapElem.insertAdjacentHTML('afterbegin',`<div class="float-right info-bubble" id="banderol"><div class="container"><div class="row"><div class="container container-img" style="background: url(<?php echo $img; ?>) 50% calc(50% + 143px) / calc(100% + 190px) no-repeat;"><div class="top-right"><a href="#" id="X">X</a></div></div></div><br><div class="row"><div class="container"><p> Nom : <?php echo $name; ?></p><p> Genre : <?php echo $sexe; ?></p><p> Date de naissance : <?php echo $dateNaissance; ?></p><p> Date de mort : <?php echo $dateMort; ?></p><p> Description : <?php echo $description; ?></p><p> Lien : <?php echo $link; ?></p></div></div></div></div>`);
                            else {
                                banderol.remove();
                                mapElem.insertAdjacentHTML('afterbegin',`<div class="float-right info-bubble" id="banderol"><div class="container"><div class="row"><div class="container container-img" style="background: url(<?php echo $img; ?>) 50% calc(50% + 143px) / calc(100% + 190px) no-repeat;"><div class="top-right"><a href="#" id="X">X</a></div></div></div><br><div class="row"><div class="container"><p> Nom : <?php echo $name; ?></p><p> Genre : <?php echo $sexe; ?></p><p> Date de naissance : <?php echo $dateNaissance; ?></p><p> Date de mort : <?php echo $dateMort; ?></p><p> Description : <?php echo $description; ?></p><p> Lien : <?php echo $link; ?></p></div></div></div></div></div>`);
                            }
                            // Fonction faire disparaître la banderol en cliquant sur le X

                            var x = document.getElementById("x");
                            var y = document.getElementById("y");
                            var nomObjet = document.getElementById("nomObjet");

                            x.setAttribute("value",this._latlng.lng);
                            y.setAttribute("value",this._latlng.lat);
                            for (var i = 0; i<nomObjet.options.length;i++){
                                if (nomObjet.options[i].value == "<?php echo $nameNonFormel ?>"){
                                    nomObjet.options[i].selected = true;
                                    break;
                                }
                            }

                            var valider = document.getElementById("modifierAjouter");
                            valider.value = "Modifier";
                            valider.name = "modifier";
                            var supprimer = document.getElementById("supprimer");
                            supprimer.style.display = "inline";

                            //nomObjet.setAttribute("value","<?php //echo $name; ?>");

                            var x = document.getElementById('X');
                            banderol = document.getElementById('banderol');

                            var idMarker = document.getElementById("idMarker");

                            x.addEventListener('click',function(){
                                banderol.remove();
                            });


                        });

                <?php
                        }
                ?>

                var tab = [];
                function onMapClick(e) {
                    //console.log(typeof marker == null);
                    //if (typeof marker != undefined) console.log("caca");
                    // map.removeLayer(marker);

                    var valider = document.getElementById("modifierAjouter");
                    valider.value = "Ajouter";
                    valider.name = "ajouter";
                    var supprimer = document.getElementById("supprimer");
                    supprimer.style.display = "none";

                    var nomObjet = document.getElementById("nomObjet");
                    nomObjet.options[0].selected = true;
                    

                    var marker = L.marker([e.latlng.lat,e.latlng.lng],{icon:coolIcon});
                    if (tab.length == 1) {
                        var oldMarker = tab[0];
                        map.removeLayer(oldMarker);
                    }
                    tab[0] = marker;
                    marker.addTo(map);
                    var x = document.getElementById("x");
                    var y = document.getElementById("y");
                    
                    x.setAttribute("value",Math.round(e.latlng.lng));
                    y.setAttribute("value",Math.round(e.latlng.lat));
                }

                map.on('click', onMapClick);
            
            </script>

            Vous pouvez mettre un marquer :
            <form method="POST">
                Coordonne X <input type="number" min="0" max="1000" name="X" id="x"> <br>
                Coordonne Y <input type="number" min="0" max="400" name="Y" id="y"> <br>
                
                Nom de l'objet 
                <select id="nomObjet" name="object">
                    <option value="">---Objet Historique---</option>
                    <?php
                        $sql = "SELECT DISTINCT(`name`) FROM `objet` NATURAL JOIN opendata";
                        $sth = $dbh->query($sql);  
                        $result = $sth->fetchAll();
                        for($i = 0;$i<count($result);$i++){
                    ?>        
                            <option value="<?php echo $result[$i][0]; ?>"><?php echo $result[$i][0]; ?></option>
                    <?php } ?>
                </select> <br>
                <!-- Nom du créateur <input type="text" name="creator"> <br> -->
                <!-- Date de création <input type="date" name="creationDate"> <br> -->
                <input type ="hidden" name="map" value="<?php echo $fileMap ?>">
                <input id="modifierAjouter" type="submit" value="Ajouter" name="ajouter">
                <input id="supprimer" type="submit" value="Supprimer" name="supprimer" style="display:none">
            </form>

            <?php 
            }
            ?>
    
    

</body>



