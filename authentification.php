<?php
	session_start ();

	include("include/connexion.php");
	function redirection($url){
		if (headers_sent()){
			print('<meta http-equiv="refresh" content="0;URL='.$url.'">');
		} else {
			header("Location:$url");
		}
	}
	// on effectue une redirection vers la page d'accueil

	if (isset($_POST["login"]) && isset($_POST["password"])){
		$login = $_POST["login"];
		$mot = $_POST["password"];
		$mdp = hash("sha1", $mot, false);
		$flag = false;
		$sql= "SELECT login, pwd_hash, type FROM user WHERE login = '$login'";
		$sth = $dbh->query($sql); //Nouvel objet, $sth $sth->execute(array(':t' => 150, ':p' => 120));
		while( $ligne = $sth->fetch(PDO::FETCH_OBJ) ) {  // un par un

			if ($ligne->pwd_hash == $mdp && $ligne->login == $login){
				$flag = true;
				$_SESSION['login'] = $_POST['login'];
				$_SESSION['password'] = $_POST['password'];
				//echo " Authentification réussie ";
				if ($ligne->type == 1) {
					//admin
					$_SESSION['admin'] = "admin";
					redirection('admin.php');
				}
				if ($ligne->type == 0) {
					//contributeur
					$_SESSION['contri'] = "contri";
					redirection('contributeur.php');
				}
			}
			
			else {
				$flag = true;
				//echo "<div align=\"center\">mot de passe incorrect </div>";
				redirection ('admin_connexion.php');
			}
			$sth->closeCursor();
		}
		if ($flag==false){
			//echo "<div align=\"center\">login incorrect</div>";
			redirection ('admin_connexion.php');
			$sth->closeCursor();
		}

	}
?>
