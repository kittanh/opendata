<?php
session_start();
if (!isset($_SESSION['contri'])) {
    header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
    exit();
}
include("include/header_contributeur.php");
?>

    <body>
        <div class="container">
            <div class="row">
                <br>
                <br>
                <br>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <h5 class="title"> Page contributeur </h5>
                </div>
            </div>
            <div class="row">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
            <div class="row text-center">
                <!-- Les trois fonctions que le contributeur à accès donc : insertion d'objet historique, insertion d'open data et insertion des markers -->      
                <div class="col-4">
                    <a href="insert_history_object.php"><button class="btn btn-primary">Insertion d'objet historique</button></a>
                </div>  
                <div class="col-4">
                    <a href="insert_open_data.php"><button class="btn btn-primary">Insertion de lien open data</button></a>
                </div>
                <div class="col-4">
                    <a href="insert_marker.php"><button class="btn btn-primary">Insertion de marker sur une carte</button></a>
                </div>
            </div>
        </div>
    </body>


<?php 
include("include/footer.php");
?>