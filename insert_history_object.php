<!DOCTYPE html>
<?php

session_start();
if (!isset($_SESSION['admin']) && !isset($_SESSION['contri'])) {
	header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
	exit();
}

// On garde l'info pour rediriger vers la bonne page
if (isset($_SESSION['admin'])) {
	$page = "addElement.php";
} else {
	$page = "contributeur.php";
}


include("include/connexion.php");
require("class/History_object.php");

?>
<html lang="fr">
<head>
	<title>Insérer un objet historique</title>
	<?php
	include("include/header_contributeur.php");// Notre page admin de connexion
	?>
</head>
<body>
  	<div class="container enleveMarge">
		<div class="row">
			<br>
		</div>
		<div class="row">
			<div class="col-4 text-left">
				<a href="<?php echo $page ?>"><button class="btn btn-primary">Revenir à la page des options</button></a>
			</div>
			<div class="col-4 text-center">
				<h5> Ajouter un objet historique dans la base de donnée </h5>
			</div>
			<div class="col-4"></div>
		</div>
		<div class="row">
			<br>
			<br>
			<br>
			<br>
		</div>

		<div class="row">
			<div class="col-4 text-left">
				<p>Liste de tous les objets historiques</p>
				<?php 

					// Ajout 
					$string = "";
					if (!empty($_POST['name']) && !empty($_POST['type'])){

						$dejaPresent = false;
                        $sql = "SELECT `name` FROM objet";
                        $result = $dbh->query($sql)->fetchAll();
                        for ($i = 0;$i < count($result);$i++){
                            if ($result[$i]['name'] == $_POST['name']){
                                $dejaPresent = true;
                            }
						}
						if ($dejaPresent) $string = "\nCe nom d'objet historique est déjà présent dans la BDD, veuillez en choisir un autre";
						else {
							$object = new History_object($_POST['name'], $_POST['type']);
							$string = $object->insert();
						}
					} else if (!empty($_POST['name'])){
						$string = "\nVous devez rentrer un type à votre objet historique";
					} else if (!empty($_POST['type'])){
						$string = "\nVous devez rentrer un nom à votre objet historique";
					}
				
					// Gestion de la suppression
					if (!empty($_POST['idH']) && !empty($_POST['supprimer'])){

						$idH = $_POST['idH'];
						$del = $dbh->prepare("DELETE FROM `objet` WHERE `name` = '$idH'");
						try{
							if($del->execute()){

							}
						}
						catch(Exception $e){
							echo "<p>" . $e->getMessage() . "</p>";
						}
					}

					// Modification 
					if (!empty($_POST['validerModif'])) {
						$request = "UPDATE objet SET name = '$_POST[newName]' WHERE `name` = '$_POST[oldName]'";
						$dbh->exec($request);
					}

				?>
				<form method="POST"> 
					<?php
					// Ce qui serait pas mal c'est d'afficher les objets historiques qui sont déjà dans la BDD

						$requete = "SELECT `name`,`type` FROM objet";
						$result = $dbh->query($requete)->fetchAll();
						for ($i = 0; $i < count($result); $i++){
							echo $result[$i]['name']. " : ";
							echo $result[$i]['type']. "    ";
							$name = $result[$i]['name'];
							echo "<input type='radio' name='idH' value='$name'><br>";
						}
					?>
					<br>
					<input type='submit' value='Modifier' name ="modifier">
					<input type='submit' value='Supprimer' name="supprimer">
				</form>
				<br>
				<?php 
					if (!empty($_POST['idH']) && !empty($_POST['supprimer'])){
						echo "Suppression réussie";
					}
				?>
			</div>
			<div class="col-4">
				<form action="insert_history_object.php" method="post">
					<p>
					Insérer un objet historique dans la base de donnée <br />
					<input type="text" name="name"/> Nom de l'objet <br />
					<select name="type">
						<option value="">---type de l'objet---</option>
						<option value="personnage">Personnage</option>
					</select>

					</p>
					<p>
						<input type="reset" name="reset" value="Effacez" />
						<input type="submit" name="submit" value="Validez" />
					</p>
				</form>
				<br>
				<?php
					echo $string;
				?>
			</div>
			<div class="col-4">
				<?php 
					if (!empty($_POST['idH']) && !empty($_POST['modifier'])){
						// Gérer la modification
				?>
						<form action="insert_history_object.php" method="post">
							<p>
							Modifier objet historique dans la base de donnée <br>
							<input type="hidden" name="oldName" value="<?php echo $_POST['idH'] ?>">
							<input type="text" name="newName" value="<?php echo $_POST['idH'] ?>"/> Nom de l'objet <br>
							<select name="type">
								<option value="">Personnage</option>
							</select>

							</p>
							<p>
								<input type="submit" name="validerModif" value="Validez les modifications" />
							</p>
						</form>
				<?php
					}
				?>
			</div>
		</div>
  </div>
</body>
<?php
	include("include/footer.php");
?>
</html>
