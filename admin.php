<?php
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
    exit();
}
include("include/header.php");
?>

    <body>
        <div class="container">
            <div class="row">
                <br>
                <br>
                <br>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <h5 class="title decalageGauche"> Page Admin </h5>
                </div>
            </div>
            <div class="row">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
            <div class="row">
                <div class="col-6 text-center">
                    <a href="gestion_compte.php"><button class="btn btn-primary">Gestion des comptes</button></a>
                </div>
                <div class="col-6 text-center">
                    <a href="addElement.php"><button class="btn btn-primary">Participation au système de cartographie</button></a>
                </div>
            </div>
        </div>
    </body>
    <?php 
        include('include/footer.php');
    ?>

</html>