<?php
session_start();
include("include/connexionLocal.php");
require("class/Open_data.php");

?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Test</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="timeline.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
            integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
            crossorigin=""></script>

</head>
<body>

<header>
    <!-- Image and text -->

    <nav class="navbar navbar-dark black">

        <ul class="nav">
            <li>
                <a class="navbar-brand" href="#">
                    <img src="image/logo_mini.png" width="80" height="80" class="d-inline-block align-middle" alt="" loading="lazy">
                    <i> IMMERSAILLES </i>
                </a>
            </li>
        </ul>


        <!-- <span class="navbar-text white">
          Immersailles
        </span>
    -->

        <!-- <div class="navbar-text"> -->
        <!-- Changez les images pour mettre des ronds blancs pour symboliser la connexion -->

        <div>
            <a href="" class="white d-inline-block align-middle">A PROPOS</a>


            <a href="admin_connexion.php"><span class="dot d-inline-block align-middle"></span></a>


            <span class="dot d-inline-block align-middle"></span>
        </div>



        <!-- </div> -->

    </nav>

</header>
<body>

<?php
    $od = new Open_data("https://www.wikidata.org/wiki/Q7742", "LOUIS XIV");
    $objet = $od->getObjet();
    echo '<img src ="' . $od->getImage($objet) . '">';
    echo '<h4>'.$od->getName($objet).'</h4>';
    echo 'Sexe: '.$od->getGender($objet);
    echo '<br>Date de naissance: '.$od->getDateBirth($objet);
    echo '<br>Date de mort: '.$od->getDateDeath($objet);
    echo '<br>Description: '.$od->getDescription($objet);
    echo '<br>Liens: '.$od->getLink($objet);
?>


</body>
</html>