<?php
	session_start();
	include("class/Open_data.php");
    include("include/connexion.php");
     //include("include/header.php");
     // à remplacer par header pour les endroits ou nous utilisons le système de carto
?>
<!-- ajouter dans map, pas de téléchargement mais juste l'ajout 
   des map qui sont dans plan dans la bddn, level plutôt un String-->
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Immersailles</title>
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="timeline.css">
	<link rel="stylesheet" href="timeline_etage.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>

  </head>
  <body>

  	<header>
      <!-- Image and text -->
    
		<nav class="navbar navbar-dark black">
			<ul class="nav">
				<li>
					<a class="navbar-brand" href="#">
					<img src="image/logo_mini.png" width="50" height="50" class="d-inline-block align-middle" alt="" loading="lazy"> 
					<i> IMMERSAILLES </i>
					</a>
				</li>
			</ul>


			<!-- <span class="navbar-text white">
				Immersailles
			</span>
		-->
			
				<!-- <div class="navbar-text"> -->
				<!-- Changez les images pour mettre des ronds blancs pour symboliser la connexion -->
				
			<div>
				<a href="" class="white d-inline-block align-middle">A PROPOS</a>
	
	
				<a href="admin_connexion.php"><span class="dot d-inline-block align-middle"></span></a>
	
	
				<span class="dot d-inline-block align-middle"></span>
			</div>   

				<!-- </div> -->
			
		</nav>
	</header>

	<!-- Initialisation des infos de base de la BDD pour savoir quelle carte exploite et etage correspondants -->
	<?php
		// Initialisation de la carte de base à l'étage RDC et à la date 1648

		$requeteComplete = "SELECT `year`,`level`,`name` FROM map NATURAL JOIN `year`";
		$request = $dbh->query($requeteComplete)->fetchAll();

		// Plus petit etage, plus petite date
		$tab = array();
		$tab = $request;

		// Trier pour que ça soit la première date et le premier étage qui soit affiché au début
		sort($tab);

		$saveEtage = $tab[0]['level'];
		$saveDate = $tab[0]['year'];

		$requeteMap = "SELECT `name` FROM map NATURAL JOIN `year` WHERE `level` = '$saveEtage' AND `year` = $saveDate";
		$result = $dbh->query($requeteMap)->fetch();
		$currentMap = $result['name'];

		//var_dump($tab[0][2]);

		// Au debut on utilise les infos $tab[0][info], 
		// puis après si on veut changer d'étages on fait 

		//$tmpTab = $tab[0];

	?>
	<!-- Fin le l'initialisation des infos --> 

	<!-- Liste des titres -->

	<div class="row" style="margin-right:0px;">
		<div class="col-lg">
			<div class="row">
				<span style="font-size:20px;" class="pl-4">
					Trier par
				
					<ul class="nav2"> 
						<li> <a href> Tout (330) </a> </li>
						<li> <a href> Oeuvres d'art (120) </a> </li>
						<li> <a href> Mobilier (100) </a> </li>
						<li> <a href> Décoration (110) </a> </li>
					</ul>
				</span>
			</div>
		</div>
		<div class="col-lg text-center">
			<!-- On peut possiblement mettre le reigne ici -->
			<p id="currentTitle" class="gras">
			<?php 
				
				$sql = "SELECT `level`,`year`,reign FROM map NATURAL JOIN `year` WHERE map.name = '$currentMap'";
				$result = $dbh->query($sql)->fetch();
				$titleYear = $result['year'];
				$titleLevel = $result['level'];
				$titleReign = $result['reign'];
				// Il faut associer la bonne source
				echo "Année ".$titleYear.", reigne de ".$titleReign.", ".$titleLevel;
				
			?>
			</p>
		</div>
		
		<div class="col-lg text-right">
			<p id="source">
			<?php 
				
				$sql = "SELECT source FROM map WHERE map.name = '$currentMap'";
				$result = $dbh->query($sql)->fetch();
				$source = $result[0];
				// Il faut associer la bonne source
				echo "Source carte : ".$source;
				
			?>
			</p>
		</div>
	</div>

    <!-- fin de la liste des titres -->


  	<!-- La carte et les etages -->
    <div id="map" class="test">

		<!-- La liste des étages -->
		<div class="flex-parent2">
			<!-- Les points ne se connectent pas à cause de la taille du container -->
			<ul id="listeEtages">
				<?php 
					// Pour être exact ce que l'on veut c'est les niveaux des maps de la map actuelle donc plutôt une requête du style
					$requete = "SELECT `level` FROM `map` NATURAL JOIN `year` WHERE `year` = $saveDate";
					$result = $dbh->query($requete)->fetchAll();
					// Je vais opter pour un système plus simple pour la liste des étages, une simple liste non ordonnée, centré sur la gauche fera l'affaire
					for($i = 0;$i<count($result);$i++){
				?>
						<!--
						<input type="radio" name="timeline-dot" onClick="" data-description="<?php //echo $result[$i]['year'] ?>">
						<div class="dot-info" data-description="1910">
							<span class="year"><?php //echo $result[$i]['year'] ?></span>
						</div>
						-->

						<li><a id="<?php echo $result[$i]['level'] ?>" href="#" onclick="changeEtage('<?php echo $result[$i]['level'] ?>')"><?php echo $result[$i]['level'] ?></a></li>
				<?php
					}
				?>		
			</ul>
		</div>

		<!-- Le popup opendata -->

		
	</div>

	<!-- fin des elements cartes et etages -->

	<!-- affichage de la carte et affichage des markers avec leurs evenements associe -->

    <script type="text/javascript">

		// Faire une array qui contient toutes les map avec les markers associé 
		// Gestion de la carte et ajout marker 
		var bounds = [[0,0], [400,1000]];

		var map = L.map('map',{
    		crs: L.CRS.Simple,
			minZoom:1,
			maxBounds:bounds
		});

		var image = L.imageOverlay('image/plan/<?php echo $currentMap ?>', bounds).addTo(map);
		map.fitBounds(bounds);

		var coolIcon = L.icon({
			iconUrl: 'image/icon.png'
		});

		<?php
			
			$sql = "SELECT `x`,`y`,idO FROM `marker`,map WHERE marker.idMap = map.idMap && map.name = '$currentMap'";
			$sth = $dbh->query($sql); 
			$result = $sth->fetchAll();
		?>
			var coord = [];
			var layerGroup = L.layerGroup().addTo(map);
			map.setView([200,500],1);
		<?php

			
			// Pour pouvoir enlever tous les markers 

			for($i = 0;$i<count($result);$i++){
				$x = $result[$i]['x'];
				$y = $result[$i]['y'];
				$idO = $result[$i]['idO'];
				
				$sql = "SELECT `name`,`type` FROM objet WHERE idO = $idO";
				$info = $dbh->query($sql)->fetch();
				$name = $info['name'];
				//$type = $info['type'];
		?>
				
				coord = [<?php echo $y; ?>,<?php echo $x; ?>];
				
		<?php
				$sql = "SELECT `url` FROM opendata WHERE idO = $idO";
				$res = $dbh->query($sql)->fetch();
				$url = $res['url'];
				
				
				$od = new Open_data("$url", "$name");
				$objet = $od->getObjet();
				$img = $od->getImage($objet);
				$name = $od->getName($objet);
				$sexe = $od->getGender($objet);
				$dateNaissance = $od->getDateBirth($objet);
				$dateMort = $od->getDateDeath($objet);
				$description = $od->getDescription($objet);
        		$link = $od->getLink($objet);
		?>
				

				var mapElem = document.getElementById("map");
				marker = L.marker(coord,{icon:coolIcon}).on('click',function(e){
					// Ici récupérer les informations opendata de l'objet et l'envoyer 
					//console.log('<?php //echo "$name"; ?>')


					var banderol = document.getElementById('banderol');

					if (banderol == null) mapElem.insertAdjacentHTML('afterbegin','<div class="float-right info-bubble" id="banderol"><div class="container"><div class="row"><div class="container container-img" style="background: url(<?php echo $img; ?>) 50% calc(50% + 143px) / calc(100% + 190px) no-repeat;"><div class="top-right"><a href="#" id="X">X</a></div></div></div><br><div class="row"><div class="container"><p> Nom : <?php echo $name; ?></p><p> Genre : <?php echo $sexe; ?></p><p> Date de naissance : <?php echo $dateNaissance; ?></p><p> Date de mort : <?php echo $dateMort; ?></p><p> Description : <?php echo $description; ?></p><p> Lien : <?php echo $link; ?></p></div></div></div></div>');
					else {
						banderol.remove();
						mapElem.insertAdjacentHTML('afterbegin','<div class="float-right info-bubble" id="banderol"><div class="container"><div class="row"><div class="container container-img" style="background: url(<?php echo $img; ?>) 50% calc(50% + 143px) / calc(100% + 190px) no-repeat;"><div class="top-right"><a href="#" id="X">X</a></div></div></div><br><div class="row"><div class="container"><p> Nom : <?php echo $name; ?></p><p> Genre : <?php echo $sexe; ?></p><p> Date de naissance : <?php echo $dateNaissance; ?></p><p> Date de mort : <?php echo $dateMort; ?></p><p> Description : <?php echo $description; ?></p><p> Lien : <?php echo $link; ?></p></div></div></div></div>');
					}
					// Fonction faire disparaître la banderol en cliquant sur le X

					var x = document.getElementById('X');
					banderol = document.getElementById('banderol');

					x.addEventListener('click',function(){
						banderol.remove();
					});
				});

				layerGroup.addLayer(marker);

		<?php		
			}

		?>

	</script>

	<!-- fin de l'affichage de la carte et affichage des markers avec leurs evenements associe -->
    

  	<!-- la timeline -->
	<?php
	  	$requete = "SELECT DISTINCT(`year`) FROM map NATURAL JOIN `year`";
		$result = $dbh->query($requete)->fetchAll();

		// Pour mettre la timeline par ordre croissant
		sort($result);
	?>
	<br>
	<div class="flex-parent">
		<div class="input-flex-container" style="width:<?php echo 9*count($result); ?>vw;">
		<!-- Les points ne se connectent pas à cause de la taille du container -->
		<?php 
			
			for($i = 0;$i<count($result);$i++){


				if ($result[$i]['year'] == $saveDate) {
		?>
					
					<input id="<?php echo $result[$i]['year'] ?>" checked="true" type="radio" name="timeline-dot" onclick="changeAnnee(<?php echo $result[$i]['year'] ?>)" data-description="<?php echo $result[$i]['year'] ?>">
					<div class="dot-info" data-description="1910">
						<span class="year"><?php echo $result[$i]['year'] ?></span>
					</div>
		<?php
				} else {
		?>
					<input id="<?php echo $result[$i]['year'] ?>" type="radio" name="timeline-dot" onclick="changeAnnee(<?php echo $result[$i]['year'] ?>)" data-description="<?php echo $result[$i]['year'] ?>">
					<div class="dot-info" data-description="1910">
						<span class="year"><?php echo $result[$i]['year'] ?></span>
					</div>			
	<?php
				}
			}
	?>		
		</div>
	</div>

	<!-- fin timeline -->

	<!-- Une div qui va me permettre de conserver l'information sur la date actuelle -->

		<input id="saveDate" name="<?php echo $saveDate ?>" type="hidden">

	<!-- -->
	
	
	<!-- interaction avec la timeline -->

	<script type="text/javascript"> 
		

		// Ces deux fonctions ci dessous me permette de compacter mon code 

		function majMap(currentMap){

			layerGroup.clearLayers();
			var image = L.imageOverlay('image/plan/' + currentMap, bounds).addTo(map);
			map.fitBounds(bounds);
		}

		function majMarker(x,y,img,name,sexe,dateN,dateM,description,link){

			console.log(x,y);
			coord = [y,x];
				

			var mapElem = document.getElementById("map");
			marker = L.marker(coord,{icon:coolIcon}).on('click',function(e){

				// Ici récupérer les informations opendata de l'objet et l'envoyer 
				//console.log('<?php //echo "$name"; ?>')


				var banderol = document.getElementById('banderol');
				// Alors je ne l'explique pas, si j'essaie de rajouter la description les intéractions avec la timeline et la liste des étages ne fonctionnent plus ...
				if (banderol == null) mapElem.insertAdjacentHTML('afterbegin','<div class="float-right info-bubble" id="banderol"><div class="container"><div class="row"><div class="container container-img" style="background: url(' + img + ') 50% calc(50% + 143px) / calc(100% + 190px) no-repeat;"><div class="top-right"><a href="#" id="X">X</a></div></div></div><br><div class="row"><div class="container"><p> Nom : ' + name + '</p><p> Genre : ' + sexe + '</p><p> Date de naissance : ' + dateN + '</p><p> Date de mort : ' + dateM + '</p><p> Description : ' + description + '</p><p> Lien : ' + link + '</p></div></div></div></div>');
				else {
					banderol.remove();
					mapElem.insertAdjacentHTML('afterbegin','<div class="float-right info-bubble" id="banderol"><div class="container"><div class="row"><div class="container container-img" style="background: url(' + img + ') 50% calc(50% + 143px) / calc(100% + 190px) no-repeat;"><div class="top-right"><a href="#" id="X">X</a></div></div></div><br><div class="row"><div class="container"><p> Nom : ' + name + '</p><p> Genre : ' + sexe + '</p><p> Date de naissance : ' + dateN + '</p><p> Date de mort : ' + dateM + '</p><p> Description : ' + description + '</p><p> Lien : ' + link + '</p></div></div></div></div>');
				}

				// Fonction faire disparaître la banderol en cliquant sur le X

				var x = document.getElementById('X');
				banderol = document.getElementById('banderol');

				x.addEventListener('click',function(){
					banderol.remove();
				});
			});

			layerGroup.addLayer(marker);
			return;
			
		
		}	
			
			
		
		
		// Dernier problème à gérer pour avoir une version fonctionnelle d'après moi, c'est en aillant plusieurs années sauvegarder le changement d'année, donc $saveDate

		function changeEtage(etage){
			// La variable save date est changé dans la fonction donc les changements ne sont pas globaux, il faudrait donc que je trouve un moyen que ceci deviennent globaux par exmple en utilisant des returns, à voir comment faire 
			var elementSaveDate = document.getElementById('saveDate');
			var saveDate = elementSaveDate.name;
			console.log(saveDate);
			<?php 
				for ($i = 0; $i<count($tab);$i++){
			?>	
					if (etage == '<?php echo $tab[$i]['level'] ?>'){
						
						
						if (saveDate == <?php echo $tab[$i]['year'] ?>) {
						<?php
							$saveEtage = $tab[$i]['level'];
							$currentMap = $tab[$i]['name'];
							echo "console.log('$currentMap');";
							$sql = "SELECT `x`,`y`,idO FROM `marker`,map WHERE marker.idMap = map.idMap && map.name = '$currentMap'";
							$sth = $dbh->query($sql); 
							$result = $sth->fetchAll();
						?>
							// Met à jour la carte 
							majMap('<?php echo $currentMap; ?>');
							// Changement de map
							coord = [];
							layerGroup = L.layerGroup().addTo(map);
				
							// Maj de la source 
							<?php 
								$sql = "SELECT source FROM map WHERE map.name = '$currentMap'";
								$currentSource = $dbh->query($sql)->fetch();
								$currentSource = $currentSource[0];
							?>

							var elemSource = document.getElementById('source');
							elemSource.innerHTML = "source : <?php echo $currentSource ?>";
							
							//Maj du titre au dessus de la map
							<?php
								$sql = "SELECT `level`,`year`,reign FROM map NATURAL JOIN `year` WHERE map.name = '$currentMap'";
								$currentTitle = $dbh->query($sql)->fetch();
								$titleYear = $currentTitle['year'];
								$titleLevel = $currentTitle['level'];
								$titleReign = $currentTitle['reign'];
							?>

							var elemCurrentTitle = document.getElementById('currentTitle');
							elemCurrentTitle.innerHTML = "Année <?php echo $titleYear ?>, reigne de <?php echo $titleReign ?>, <?php echo $titleLevel ?>";

							<?php

								
								// Pour pouvoir enlever tous les markers 
								
								for($j = 0;$j<count($result);$j++){

									$x = $result[$j]['x'];
									$y = $result[$j]['y'];
									$idO = $result[$j]['idO'];
									
									$sql = "SELECT `name`,`type` FROM objet WHERE idO = $idO";
									$info = $dbh->query($sql)->fetch();
									$name = $info['name'];

									$sql = "SELECT `url` FROM opendata WHERE idO = $idO";
									$res = $dbh->query($sql)->fetch();
									$url = $res['url'];
									
									
									$od = new Open_data("$url", "$name");
									$objet = $od->getObjet();
									$img = $od->getImage($objet);
									$name = $od->getName($objet);
									$sexe = $od->getGender($objet);
									$dateNaissance = $od->getDateBirth($objet);
									$dateMort = $od->getDateDeath($objet);
									$description = $od->getDescription($objet);
									$link = $od->getLink($objet);
													
							?>
									// Met à jour les markers 
									majMarker(<?php echo $x ?>,<?php echo $y ?>,'<?php echo $img ?>','<?php echo $name ?>','<?php echo $sexe ?>','<?php echo $dateNaissance ?>','<?php echo $dateMort ?>',`<?php echo $description ?>`,'<?php echo $link ?>');

							<?php
								}	
							?>
							return;
							
							} 
						
						}
				<?php	
					}
				?>
			
		}

		function changeAnnee(anne){
			//console.log('<?php //echo $saveEtage ?>');
			<?php
				
				$requeteComplete = "SELECT `year`,`level`,`name` FROM map NATURAL JOIN `year`";
				$request = $dbh->query($requeteComplete)->fetchAll();

				// Plus petit etage, plus petite date
				$tab = array();
				$tab = $request;
				
				for ($i = 0; $i<count($tab); $i++){

				/*
				$year = $tmpTab['year'];
				$etage = $tmpTab['level'];
				*/
				
			?>	
					// Quand on va changer d'année on va prendre de base le premier étage présent sur la BDD 
					if (anne == <?php echo $tab[$i]['year'] ?>){
						<?php 
						// Cette saveDate doit être sauvegarder dans le contexte global comment faire, ça va être mon combat de demain
						$saveDate = $tab[$i]['year'];
						$currentMap = $tab[$i]['name'];
						$sql = "SELECT `level` FROM map NATURAL JOIN `year` WHERE `year` = $saveDate";
						// Liste des etages pour cette annee 
						$tabEtage = $dbh->query($sql)->fetchAll();
						$saveEtage = $tabEtage[0]['level'];
						?>

						// Sauvegarde de la date actuelle 

						var elementSaveDate = document.getElementById('saveDate');
						elementSaveDate.setAttribute('name',<?php echo $saveDate ?>);

						// Changement de map
						
						majMap('<?php echo $currentMap ?>');

						// Mise à jour de la source 

						<?php 
							$sql = "SELECT source FROM map WHERE map.name = '$currentMap'";
							$currentSource = $dbh->query($sql)->fetch();
							$currentSource = $currentSource[0];
						?>

						var elemSource = document.getElementById('source');
						elemSource.innerHTML = "source : <?php echo $currentSource ?>";

						//Maj du titre au dessus de la map
						<?php
							$sql = "SELECT `level`,`year`,reign FROM map NATURAL JOIN `year` WHERE map.name = '$currentMap'";
							$currentTitle = $dbh->query($sql)->fetch();
							$titleYear = $currentTitle['year'];
							$titleLevel = $currentTitle['level'];
							$titleReign = $currentTitle['reign'];
						?>

						var elemCurrentTitle = document.getElementById('currentTitle');
						elemCurrentTitle.innerHTML = "Année <?php echo $titleYear ?>, reigne de <?php echo $titleReign ?>, <?php echo $titleLevel ?>";

						
						// Changer la liste des etages visibles 

						var listeEtages = document.getElementById('listeEtages');
						console.log(listeEtages);
						listeEtages.innerHTML = "";
						<?php
							$requete = "SELECT `level` FROM `map` NATURAL JOIN `year` WHERE `year` = $saveDate";
							$result = $dbh->query($requete)->fetchAll();
							for($j = 0;$j<count($result);$j++){
						?>
								listeEtages.innerHTML += "<li><a id='<?php echo $result[$j]['level'] ?>' href='#'"
								+ `onclick="changeEtage('<?php echo $result[$j]['level'] ?>')"><?php echo $result[$j]['level'] ?></a></li>`;
						<?php
							}
						?>

						<?php
							
							$sql = "SELECT `x`,`y`,idO FROM `marker`,map WHERE marker.idMap = map.idMap && map.name = '$currentMap'";
							$sth = $dbh->query($sql); 
							$result = $sth->fetchAll();
							
							
						?>
							
							coord = [];
							layerGroup = L.layerGroup().addTo(map);
							//map.setView([200,500],1);
						<?php

							
							// Pour pouvoir enlever tous les markers 
							
							for($j = 0;$j<count($result);$j++){

								$x = $result[$j]['x'];
								$y = $result[$j]['y'];
								$idO = $result[$j]['idO'];
								
								$sql = "SELECT `name`,`type` FROM objet WHERE idO = $idO";
								$info = $dbh->query($sql)->fetch();
								$name = $info['name'];

								$sql = "SELECT `url` FROM opendata WHERE idO = $idO";
								$res = $dbh->query($sql)->fetch();
								$url = $res['url'];
								
								
								$od = new Open_data("$url", "$name");
								$objet = $od->getObjet();
								$img = $od->getImage($objet);
								$name = $od->getName($objet);
								$sexe = $od->getGender($objet);
								$dateNaissance = $od->getDateBirth($objet);
								$dateMort = $od->getDateDeath($objet);
								$description = $od->getDescription($objet);
								$link = $od->getLink($objet);
							
						?>
								
							majMarker(<?php echo $x ?>,<?php echo $y ?>,'<?php echo $img ?>','<?php echo $name ?>','<?php echo $sexe ?>','<?php echo $dateNaissance ?>','<?php echo $dateMort ?>',`<?php echo $description ?>`,'<?php echo $link ?>');
								
						<?php		
							}
						?>
						
						return;
						// Etablir une liste des etages pour cette annee puis afficher la carte qui represente le premier etage possible 
						
					}
				<?php
				}
				?>
		}

	</script>

	<!-- fin interaction avec la timeline -->
	<br>
	<br>
	<br>
	<?php
   		include("include/footer_index.php");
   	?>

  </body>
</html>