<!DOCTYPE html>
<?php
session_start();
if (!isset($_SESSION['admin']) && !isset($_SESSION['contri'])) {
    header('Location: https://etudiant.u-pem.fr/~dalbisso/opendata/index.php');
    exit();
}

if (isset($_SESSION['admin'])) {
	$page = "addElement.php";
} else {
	$page = "contributeur.php";
}

include("include/connexion.php");
require("class/Open_data.php");

?>
<html lang="fr">
<head>
	<title>Insérer une open data pour un objet donné</title>
	<?php
    include("include/header_contributeur.php");// Notre page admin de connexion
	?>
</head>
<body>
    <div class="container enleveMarge">
        <div class="row">
            <br>
        </div>
        <div class="row">
            <div class="col-4 text-left">
                <a href="<?php echo $page ?>"><button class="btn btn-primary">Revenir à la page des options</button></a>
            </div>
            <div class="col-4 text-center">
                <h5> Ajout de lien open data dans la base de donnée </h5>
            </div>
            <div class="col-4"></div>
        </div>
        <div class="row">
            <br><br><br><br><br><br>
        </div>
        <div class="row">
            <div class="col-5 text-left">
                <p>Liste de toutes les open data</p>
                <?php
                $requete = "SELECT `name`,`url` FROM opendata NATURAL JOIN objet";
                $result = $dbh->query($requete)->fetchAll();
                for ($i = 0; $i < count($result); $i++){
                    echo $result[$i]['name']. " : ";
                    echo "<a href='".$result[$i]['url']."'>".$result[$i]['url']. "</a><br>";
                }
                ?>
            </div>
            <div class="col-5">
                <form action="insert_open_data.php" method="post">
                    <p>
                        Insérer une open data pour un objet donné dans la base de données <br />
                        <input type="url" name="url"/> Lien de l'open data <br />
                        <select name="name_object">
                            <option value="">---Nom de l'objet---</option>
                            <?php
                                $sql = "SELECT `name` FROM `objet`";
                                $sth = $dbh->query($sql); 
                                //$row = $sth->fetch_assoc();
                                $result = $sth->fetchAll();
                                for($i = 0;$i<count($result);$i++){
                            ?>        
                                    <option value="<?php echo $result[$i][0]; ?>"><?php echo $result[$i][0]; ?></option>
                            <?php } ?>
                        </select>

                    </p>
                    <p>
                        <input type="reset" name="reset" value="Effacez" />
                        <input type="submit" name="submit" value="Validez" />
                    </p>
                </form>
                <?php
                    if (!empty($_POST['url']) && !empty($_POST['name_object'])){
                        
                        // Verification si ce lien est déjà présent dans la bdd 

                        $dejaPresent = false;
                        $sql = "SELECT `url` FROM opendata NATURAL JOIN objet";
                        $result = $dbh->query($sql)->fetchAll();
                        for ($i = 0;$i < count($result);$i++){
                            if ($result[$i]['url'] == $_POST['url']){
                                $dejaPresent = true;
                            }
                        }

                        if (count(explode("/", $_POST['url'])) != 5){
                            echo "\nLe lien que vous avez rentré n'est pas utilisable";
                        } else if($dejaPresent){
                            echo "\nCette url est déjà présent dans la base de donnée";
                        } else {
                            $open = new Open_data($_POST['url'], $_POST['name_object']);
                            $open->insert();
                            echo "\n Votre lien open data a bien été ajouté à la base de donnée";
                        }
                        
                    } else if (!empty($_POST['url'])) {
                        echo "\nVeuillez renseigner un objet à associé à votre url";
                    } else if (!empty($_POST['name_object'])) {
                        echo "\nVeuillez renseigner un url";
                    }
                ?>
            </div>
        </div>
    </div>
</body>
<?php
	include("include/footer.php");
?>
</html>